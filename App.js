import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import axios from "axios";
import MovieCard from './src/shared/movieCard/MovieCard';

export default class App extends Component {
  state = {
    movies: [],
    loading: true,
    page: 1,
  }

  componentDidMount() {
    let url = "https://api.themoviedb.org/3/movie/popular?api_key=53c6bcb2746b42c20afb925c1e79ce51&page=" + this.state.page;
    axios.get(url).then((response) => {
      setTimeout(() => {
        this.setState({
          movies: this.state.movies.concat(response.data.results),
          loading: false

        },
        );
      }, 2000)

    })
  } //fecha componente DidMouth


  reloadpage() {
    this.setState({
      page: this.state.page + 1
    }, () => {
      this.componentDidMount()
    })
  }

  render() {

    return (
      <View style={styles.container}>

        <View style={styles.loadingConfig2}>
          {this.state.loading ?
            <View>
              <Text style={styles.loadingTitle}>Loading</Text>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          : null}

        </View>

        <View style={styles.movie}>
          <FlatList
            data={this.state.movies}
            renderItem={({ item }) => <MovieCard movies={item} />}
            keyExtractor={(item) => item.id.toString()}
            onEndReached={() => {
              this.reloadpage()
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#DCDCDC"
  },
  movie: {
    justifyContent: "center",
    alignItems: 'center'
  },
  loadingTitle: {
    fontSize: 25,
  },
  loadingConfig2: {
    justifyContent: "center",
    alignItems: 'center',
    flex: 1,
  }

});
