import React from 'react';
import { StyleSheet, Text, View, Image, } from 'react-native';
import moment from "moment";

export default class MovieCard extends React.Component {

  render() {
    var movies = this.props.movies
    return (
      <View style={styles.card}>
        <View
          style={{
            height: 150,
            width: "100%",
            alignItems: "center"
          }}>
          <Image style={styles.ImageConfig} resizeMode={'cover'} source={{ uri: "https://image.tmdb.org/t/p/w500" + this.props.movies.backdrop_path }} />
        </View>
        <Text style={styles.textConfig}>{this.props.movies.title}</Text>
        <Text>{moment(this.props.movies.release_date).format("DD/MM/YYYY")}</Text>
        <Text style={styles.overviewConfig}>{this.props.movies.overview}</Text>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: "#fff",
    flex: .2,
    justifyContent: "flex-start",
    alignItems: "center",
    borderRadius: 10,
    margin: 15,
    padding: 15,
    shadowOpacity: 0.30,
    shadowRadius: 10,
    shadowColor: "black",
    shadowOffset: { height: 3, width: 0 },
    elevation: 4
  },
  ImageConfig: {
    width: "104%",
    height: 150,
  },
  textConfig: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  overviewConfig: {
    fontSize: 15,
    paddingBottom: 10,
  }
});
